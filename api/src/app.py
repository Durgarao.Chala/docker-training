from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html', data=None, res="")


@app.route('/fibonacci', methods=['POST'])
def fibonacci():
    num = request.form['number']
    if num:
        res = ['0', '1']
        for i in range(2, int(num)):
            res.append(str(int(res[i - 1]) + int(res[i - 2])))
        res = " ".join(res)
        return render_template('index.html', data=res, res="fib")
    else:
        return render_template('index.html', data="", res="fib")


@app.route('/factorial', methods=['POST'])
def factorial():
    num = request.form['number']
    if num and int(num) > 1:
        res = 1
        for i in range(1, int(num) + 1):
            res = res * i
        return render_template('index.html', data=str(res), res="fact")
    else:
        return render_template('index.html', data="", res="fact")
