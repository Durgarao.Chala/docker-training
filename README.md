# docker-training 
Make a Math API that has endpoints to solve the below functions
```
1.Fibonacci
2.Factorial
```

## Steps to run Math API

step1: pull the latest image from GitLab repository
```
docker pull registry.gitlab.com/durgarao.chala/docker-training/math-api:latest
```
step2:
run the image with port 5000
```
docker run --rm --name flask-math-api -p 5000:500 registry.gitlab.com/durgarao.chala/docker-training/math-api
```
It runs the server
```
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on all addresses.
   WARNING: This is a development server. Do not use it in a production deployment.
 * Running on http://172.17.0.2:5000/ (Press CTRL+C to quit)

```
step 3: Access the API either going through http://172.17.0.2:5000/

or we can goto http://localhost:5000/

![](docker-training.png)

step 4:

1. Give a number to find Fibonacci in text feild and click on ```Fibonacci``` button, it will give Fibonacci series

2. Give a number to find Factorial in text feild and click on ```Factorial``` button, it will give Factorial of given number.